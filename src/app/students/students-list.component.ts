import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Student } from '../store/student.model';
import { getFilteredStudents } from '../store/students.selectors';
import { sortStudentsByFirstName, sortStudentsByLastName, changeStudentGrade } from '../store/students.actions';



@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent  {
  public students$!: Observable<Student[]>
  public searchQuery = '';
  constructor(private store: Store<{ students: Student }>) {
    this.students$ = store.pipe(select(getFilteredStudents, ''));
    console.log(this.students$)
  }
 
  sortByFirstNameAsc() {
    this.store.dispatch(sortStudentsByFirstName());
  }
  sortByLastNameAsc() {
    this.store.dispatch(sortStudentsByLastName());
  }
  onSearch(event: any): void {
    this.searchQuery = event.target.value;
    this.students$ = this.store.pipe(
      select(getFilteredStudents, this.searchQuery)
    );
  }
  updateStudentGrade(id: number, grade:number) {
    this.store.dispatch(changeStudentGrade({id, grade}));
  }

  deleteStudentGrade(id: number) {
    const grade = 0
    this.store.dispatch(changeStudentGrade({id,  grade}));
  }
}