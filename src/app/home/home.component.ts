import { Component } from '@angular/core';
import { Student } from '../students/student-list.interface';
import { Store } from '@ngrx/store';
import { addStudent } from '../store/students.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
 constructor( private store: Store) {

 }
  onAddStudent( student : any) {
    this.store.dispatch(addStudent(student));
  }
}
