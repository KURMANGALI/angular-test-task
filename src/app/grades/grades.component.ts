import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.css']
})
export class GradesComponent {
  @Input() grade: any;
  @Output() updateStudent = new EventEmitter<any>();
  @Output() deleteStudent = new EventEmitter<any>();
  gradeForm: FormGroup;

  constructor() {
    this.gradeForm = new FormGroup({
      grade: new FormControl('', [Validators.required, Validators.min(0), Validators.max(100)])
    });
  }

  onSubmit() {
    if (this.gradeForm.valid) {
      const grade = this.gradeForm.controls['grade'].value;
      this.updateStudent.emit(grade);
      this.gradeForm.reset();
    }
  }
}
