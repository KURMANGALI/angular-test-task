import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Student } from '../students/student-list.interface';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css'],
})
export class AddStudentComponent {
  @Output() addStudent = new EventEmitter<{ firstName: string, lastName: string }>();
  studentForm: FormGroup;
  isStudentAdded = false;

  constructor(private fb: FormBuilder) {
    this.studentForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  onSubmit() {
    if (this.studentForm.valid) {
      const firstName = this.studentForm.controls['firstName'].value;
      const lastName = this.studentForm.controls['lastName'].value;
      const student: Student = {
        firstName,
        lastName,
        grade: 0,
      }
      this.addStudent.emit(student);
      this.studentForm.reset();
      this.isStudentAdded = true;
      setTimeout(() => { this.isStudentAdded = false; }, 2000);
    }
  }
}
