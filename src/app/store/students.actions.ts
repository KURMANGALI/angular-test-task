import { createAction, props } from '@ngrx/store';

export const addStudent = createAction(
  '[Students] Add Student',
  props<{  firstName: string, lastName: string, grade: number }>()
);
export const sortStudentsByFirstName = createAction(
  '[Student] Sort Students by First Name'
);

export const sortStudentsByLastName = createAction(
  '[Student] Sort Students by Last Name'
);
export const searchStudents = createAction(
  '[Students List] Search Students',
  props<{ query: string }>()
);

export const changeStudentGrade = createAction(
  '[Students] Change Student Grade',
  props<{ id: number; grade: number }>()
);
