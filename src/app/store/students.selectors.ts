import { createSelector } from '@ngrx/store';
import { StudentsState } from './students.reducer';
import { Student } from './student.model';

export const selectStudentsState = (state: any) => state.students;

export const getStudents = createSelector(
  selectStudentsState,
  (state: StudentsState) => state.students
);

export const getFilteredStudents = createSelector(
  getStudents,
  (students: Student[], name: string) => {
    return students.filter(student => student.firstName.toLowerCase().includes(name.toLowerCase()));
  }
);