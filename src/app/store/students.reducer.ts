import { Action, createReducer, on } from '@ngrx/store';
import { Student } from './student.model';
import { addStudent, changeStudentGrade, searchStudents, sortStudentsByFirstName, sortStudentsByLastName } from './students.actions';


export interface StudentsState {
  students: Student[];
  sortByFirstNameAsc: boolean;
  sortByLastNameAsc: boolean;
  searchQuery: string;
}

export const initialState: StudentsState = {
  students: [],
  sortByFirstNameAsc: true,
  sortByLastNameAsc: true,
  searchQuery: ''
};

export const studentsReducer = createReducer(
  initialState,
  on(addStudent, (state, {firstName, lastName, grade}) => {
    return {
      ...state,
      students: [...state.students, {id: state.students.length + 1, firstName, lastName, grade}]
    };
  }),
  on(sortStudentsByFirstName, (state) => {
    const sortedStudents = [...state.students].sort((a, b) => {
      if (state.sortByFirstNameAsc) {
        return a.firstName.localeCompare(b.firstName);
      } else {
        return b.firstName.localeCompare(a.firstName);
      }
    });
    return {
      ...state,
      students: sortedStudents,
      sortByFirstNameAsc: !state.sortByFirstNameAsc
    };
  }),
  on(sortStudentsByLastName, (state) => {
    const sortedStudents = [...state.students].sort((a, b) => {
      if (state.sortByLastNameAsc) {
        return a.lastName.localeCompare(b.lastName);
      } else {
        return b.lastName.localeCompare(a.lastName);
      }
    });
    return {
      ...state,
      students: sortedStudents,
      sortByLastNameAsc: !state.sortByLastNameAsc
    };
  }),
  on(searchStudents, (state, {query}) => {
    const filteredStudents = state.students.filter(student => {
      const nameMatch = student.firstName.toLowerCase().includes(query.toLowerCase());
      const surnameMatch = student.lastName.toLowerCase().includes(query.toLowerCase());
      return nameMatch || surnameMatch;
    });
    return {
      ...state,
      query,
      students: filteredStudents
    };
  }),
  on(changeStudentGrade, (state, { id, grade }) => {
    const students = state.students.map((student) => {
      if (student.id === id) {
        return { ...student, grade };
      } else {
        return student;
      }
    });
    return {
      ...state,
      students,
    };
  }),
);